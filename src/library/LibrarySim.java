
package library;

import java.util.ArrayList;
import java.util.List;


public class LibrarySim {

   
    public static void main(String[] args) {
    
        //Create Books(Top Sellers)
        Book book1 = new Book("Don Quixote", "Miguel de Cervantes");
        Book book2 = new Book("A Tale of Two Cities", "Charles Dickens");
        Book book3 = new Book("The Lord of the Rings", "J.R.R. Tolkien");
        
        //Put books into List
        List<Book> books = new ArrayList<Book>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        
        //Create Library
        Library library = new Library("Red River Public Library", books);
        
        List<Book> bks = library.getBooksInLibrary();
        for(Book bk :bks){
            System.out.println("Title: " + bk.getBookTitle()
                             + " Author: " + bk.getBookAuthor());
        }
        
    }
    
}
